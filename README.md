# yealinkutility

## A utility for Yealink phones

#### Installation:

- Clone this repository to your local machine:
```git clone https://bitbucket.org/p65/yealinkutility```
and cd into the local repo:
```cd yealinkutility```
- Create a virtual environment:
```python3 -m venv venv```
or
```virtualenv venv```
- Activate the virtual environment
```source venv/bin/activate```
- Install **yealinkutility** using pip:
```pip install .```

#### Usage:
Use the input template to create an input file. IPv4, USERNAME and PASSWORD are required fields.


```
yutil -i <inputfile.csv> -o <reportfile.csv> [COMMAND]

COMMAND:
get-data			Gets MAC address, firmware and model of phones
reboot				Reboots phones
factory-reset		Factory Resets the phones
```

#### Firmware:

This utility has been tested on the following firmware:

44.81.0.110

44.80.0.130

44.80.0.95

44.80.0.70

