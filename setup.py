from setuptools import setup

setup(name='yealinkutility',
      version='0.1',
      description='Utility for Yealink phones',
      url='https://bitbucket.org/p65/yealinkutility',
      author='P65',
      author_email='support@p65.com',
      license='MIT',
      packages=['yealinkutility'],
      install_requires=[
          'requests==2.19.1',
          'rsa==3.4.2',
          'pycrypto==2.6.1'],
      scripts=['bin/yutil'],
      zip_safe=False)
