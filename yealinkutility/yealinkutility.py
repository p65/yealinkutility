#import pycrypto
from Crypto.Cipher import AES
import hashlib
import rsa
import os
import binascii
import requests
import random
import base64
import csv
import argparse
from collections import OrderedDict
import re
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

class Report(dict):
    pass

class Key_IV:
    def __init__(self):
        key = binascii.hexlify(os.urandom(16)).decode()
        m = hashlib.md5()
        m.update(key.encode('utf-8'))
        self.string = m.hexdigest()

    def __repr__(self):
        return self.string

    def rsa_encrypt(self, pub_key_exp):
        yealink_rsa_param = 65537
        message = self.string.encode('utf-8')
        pub_key = rsa.PublicKey(int(pub_key_exp, 16), yealink_rsa_param)
        encrypted_key = rsa.encrypt(message, pub_key)
        return binascii.b2a_hex(encrypted_key)

class Data:
    def __init__(self, session_info, username, pwd):
        self.rand = (random.randint(0, 10*(10**16))/(10**17))
        data = 'rand=' + str(self.rand) + ';'
        data = data + 'sessionid=' + session_info['session_id'] + ';'
        data = data + 'username=' + username + ';'
        data = data + 'pwd=' + pwd + ';'
        m = hashlib.md5()
        m.update(data.encode('utf-8'))
        self.data = 'MD5=' + m.hexdigest() + ';' + data

    def __repr__(self):
        return self.data

    def zero_pad(self, data):
        zeroes_needed = 16 - (len(data) % 16)
        for zeroes in range(0, zeroes_needed):
            data = data + '\0'
        return data

    def aes_encrypt(self, key, iv):
        aes_mode = AES.MODE_CBC
        obj = AES.new(key, aes_mode, iv)
        padded_plaintext = self.zero_pad(self.data)
        ciphertext = obj.encrypt(padded_plaintext)
        b64_string = str(base64.b64encode(ciphertext), 'utf-8')
        return b64_string


class Phone(object):
    def __init__(self, ip):
        self.ip = ip
        self.mac_address = None
        self.firmware = None
        self.model = None
        self.token = None
        self.status = None
        self.base = 'http'
        self.result = None
        self.username = None
        self.pwd = None
        self.defaultusername = 'admin'
        self.defaultpwd = 'admin'

    def percent_encode(self, raw_string):
        percent_string = raw_string.replace('+', '%2B').replace('/', '%2F').replace('=', '%3D')
        return percent_string

    def get_session_info(self, base):
        url = base + '://' + self.ip + '/servlet?p=login&q=getsessionid'
        response = requests.get(url, verify=False)
        session_dict = {}
        session_dict['pub_exp'] = response.text.split(',')[0]
        session_dict['param'] = response.text.split(',')[1]
        session_dict['session_id'] = response.text.split(',')[2]
        self.session_info = session_dict



    def get_load_page(self, base):
        url = base + '://' + self.ip + '/servlet?p=status&q=load'
        referer = 'http://' + self.ip + '/servlet?p=login&q=loginForm&jumpto=status'
        jsession = 'JSESSIONID=' + self.session_info['session_id']
        headers = {'Host':'ip',
                   'Connection': 'keep-alive',
                   'Cache-Control': 'max-age=0',
                   'Upgrade-Insecure-Requests': '1',
                   'Referer': referer,
                   'Cookie': jsession}
        response = requests.get(url, headers=headers, verify=False)
        return response

    def get_token(self, htmlstring):
        m = re.search('(g_strToken).*', htmlstring)
        try:
            tokenstring = m.group(0)
            stringsplit = tokenstring.split('"')
            self.token = stringsplit[1]
        except AttributeError:
            try:
                m = re.search('(UpdateToken).*', htmlstring)
                tokenstring = m.group(0)
                stringsplit = tokenstring.split('"')
                self.token = stringsplit[1]
            except AttributeError:
                self.token = ''

    def get_mac(self, htmlstring):
        m = re.search('(tdMACAddress).*', htmlstring)
        macstring = m.group(0)
        stringsplit = re.split('[< >]', macstring)
        macstring = stringsplit[2]
        self.mac_address = macstring

    def get_firmware(self, htmlstring):
        m = re.search('(tdFirmware).*', htmlstring)
        firmwarestring = m.group(0)
        stringsplit = re.split('[< >]', firmwarestring)
        firmwarestring = stringsplit[2]
        self.firmware = firmwarestring

    def get_model(self, htmlstring):
        m = re.search('(<title>).*(?=(</title>))', htmlstring)
        titlestring = m.group(0)
        splitstring = titlestring.split('<title>')
        self.model = splitstring[1]

    def authenticate(self, base):
        self.get_session_info(base)
        url = base + '://' + self.ip + '/servlet?p=login&q=login'
        jsession = 'JSESSIONID=' + self.session_info['session_id']
        key = Key_IV()
        iv = Key_IV()
        key_to_send = key.rsa_encrypt(self.session_info['pub_exp'])
        iv_to_send = iv.rsa_encrypt(self.session_info['pub_exp'])
        plaintext =  Data(self.session_info, self.username, self.pwd)
        b64data = plaintext.aes_encrypt(bytes.fromhex(key.string), bytes.fromhex(iv.string))
        #do not use this raw_body for ANYTHING but to calculate content-length
        raw_body = 'key=' + key.string + '&iv=' + iv.string + '&data=' + self.percent_encode(b64data) + '&jumpto=status&acc='
        body = OrderedDict([('key', key_to_send.decode('utf-8')),
                             ('iv', key_to_send.decode('utf-8')),
                             ('data', b64data),
                             ('jumpto', 'status'),
                             ('acc', '')])
        length = str(len(raw_body))
        headers = {'Host':self.ip,
                   'Connection':'keep-alive',
                   'Cookie':jsession,
                   'Content-Type':'application/x-www-form-urlencoded',
                   'Content-Length':length,
                   'Accept':'text/html',
                   'Accept-Encoding':'gzip,deflate'}
        response = requests.post(url=url, data=body, headers=headers, allow_redirects=False, verify=False)
        #print(response)
        #print(jsession)
        self.response = response
        if response.is_redirect:
            redirect_response = self.get_load_page(base)
            self.get_token(redirect_response.text)
            self.get_mac(redirect_response.text)
            self.get_firmware(redirect_response.text)
            self.get_model(redirect_response.text)
            self.status = 'CONNECT_OK'
            #print(self.token)
            #print(redirect_response)
            #print(redirect_response.text.encode('utf-8'))
        else:
            self.status = 'NO_LOGIN'

    def reboot(self):
        url = self.base + '://' + self.ip + '/servlet?p=settings-upgrade&q=reboot'
        referer = 'http://' + self.ip + '/servlet?p=settings-upgrade&q=load'
        jsession = 'JSESSIONID=' + self.session_info['session_id']
        length = str(len('token=' + self.token))
        headers = {'Host':self.ip,
                   'Origin': self.ip,
                   'Connection':'keep-alive',
                   'Content-Length': length,
                   'Cache-Control': 'max-age=0',
                   'Upgrade-Insecure-Requests': '1',
                   'Cookie':jsession,
                   'Content-Type':'application/x-www-form-urlencoded',
                   'Referer': referer,
                   'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                   'Accept-Encoding':'gzip,deflate'}
        data ='token=' + self.token
        response = requests.post(url, headers=headers, data=data, verify=False)
        #print(response)

    def factory_reset(self):
        url = self.base + '://' + self.ip + '/servlet?p=settings-upgrade&q=resetSettings'
        referer = 'http://' + self.ip + '/servlet?p=settings-upgrade&q=load'
        jsession = 'JSESSIONID=' + self.session_info['session_id']
        data = 'type=factory&token=' + self.token
        length = str(len('token=' + self.token))
        headers = {'Host':self.ip,
                   'Origin': self.ip,
                   'Connection':'keep-alive',
                   'Content-Length': length,
                   'Cache-Control': 'max-age=0',
                   'Upgrade-Insecure-Requests': '1',
                   'Cookie':jsession,
                   'Content-Type':'application/x-www-form-urlencoded',
                   'Referer': referer,
                   'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                   'Accept-Encoding':'gzip,deflate'}
        response = requests.post(url, headers=headers, data=data, verify=False)
        #print(response)
        if response.status_code == 404:
            url = self.base + '://' + self.ip + '/servlet?p=settings-upgrade&q=reset'
            response = requests.post(url, headers=headers, data=data, verify=False)
        #print(response)


#if __name__ == "__main__":
#    print("IPv4 address:")
#    myphone = Phone('10.222.3.10')
#    myphone.authenticate('https')

class Phones():
    def __init__(self, filename):
        phonelist = []
        with open(filename, 'r') as infile:
            reader = csv.reader(infile)
            for i, line in enumerate(reader):
                if i != 0:
                    phone = Phone(line[1])
                    if line[2] != None:
                        phone.username = line[2]
                    else:
                        phone.username = self.defaultusername
                    if line[3] != None:
                        phone.pwd = line[3]
                    else:
                        phone.pwd = self.pwd
                    phonelist.append(phone)
        self.phones = phonelist
        self.reportfile = None

    def generate_report(self):
        with open(self.reportfile, 'w') as outfile:
            writer = csv.writer(outfile)
            #row = ['MAC', 'IPv4', 'MODEL', 'FIRMWARE', 'CONN_STATUS', 'RESULT']
            row = ['MAC', 'IPv4', 'MODEL', 'FIRMWARE', 'CONN_STATUS', 'RESULT']
            writer.writerow(row)
            for i, phone in enumerate(self.phones):
                    row = [phone.mac_address, phone.ip, phone.model, phone.firmware, phone.status, phone.result]
                    writer.writerow(row)

    def reboot_all_phones(self):
        for phone in self.phones:
            try:
                phone.authenticate('http')
            except requests.exceptions.ConnectionError:
                try:
                    phone.authenticate('https')
                    if phone.status == 'CONNECT_OK':
                        phone.base = 'https'
                except requests.exceptions.ConnectionError:
                    phone.status = 'NO_CONNECT'
            if phone.status == 'CONNECT_OK':
                phone.result = 'REBOOT_OK'
                phone.reboot()
            else:
                phone.result = 'NO_REBOOT'
            print(phone.ip + ',' + phone.status + ',' + phone.result)

    def factory_reset_all_phones(self):
        for phone in self.phones:
            try:
                phone.authenticate('http')
            except requests.exceptions.ConnectionError:
                try:
                    phone.authenticate('https')
                    if phone.status == 'CONNECT_OK':
                        phone.base = 'https'
                except requests.exceptions.ConnectionError:
                    phone.status = 'NO_CONNECT'
            if phone.status == 'CONNECT_OK':
                phone.factory_reset()
                phone.result = 'FACTORY_RESET_OK'
            else:
                phone.result = 'NO_FACTORY_RESET'
            print(phone.ip + ',' + phone.status + ',' + phone.result)


    def get_data_all_phones(self):
        for phone in self.phones:
            try:
                phone.authenticate('http')
            except requests.exceptions.ConnectionError:
                try:
                    phone.authenticate('https')
                    if phone.status == 'CONNECT_OK':
                        phone.base = 'https'
                except requests.exceptions.ConnectionError:
                    phone.status = 'NO_CONNECT'
            if phone.status == 'CONNECT_OK':
                phone.result = 'GET_DATA_OK'
            else:
                phone.result = 'NO_DATA'
            print(phone.ip + ',' + phone.status + ',' + phone.result)
